﻿
#include <iostream>

class Animal
{
public:
    virtual ~Animal() {}
    virtual void Voice()
    {
        std::cout << "Animal voice\n";
    }
};

class Dog : public Animal
{
public:
    virtual void Voice() override
    {
        std::cout << "Dog goes 'woof' \n";
    }

};

class Cat : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "Cat goes 'meow'\n";
    }
};

class Bird : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "Bird goes 'tweet'\n";
    }
};
class Mouse : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "And mouse goes 'squeek'\n";
    }
};
class Cow : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "Cow goes 'moo'\n";
    }
};
class Frog : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "Frog goes 'croak'\n";
    }
};
class Elephant : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "And the elephant goes 'toot'\n";
    }
};
class Duck : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "Ducks say 'quack'\n";
    }
};
class Fish : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "And fish go 'blub'\n";
    }
};
class Seal : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "And the seal goes 'ow ow ow'\n";
    }
};
class Fox : public Animal
{
public:

    virtual void Voice() override
    {
        std::cout << "What does the fox say?\nRing-ding-ding-ding-dingeringeding!\n";
    }
};

int main()
{
    
    Animal *p = new Dog();
    Animal* p1 = new Cat;
    Animal* p2 = new Bird;
    Animal* p3 = new Mouse;
    Animal* p4 = new Cow;
    Animal* p5 = new Frog;
    Animal* p6 = new Elephant;
    Animal* p7 = new Duck;
    Animal* p8 = new Fish;
    Animal* p9 = new Seal;
    Animal* p10 = new Fox;

    Animal* array[11] = {
        p, p1, p2, p3,
        p4, p5, p6, p7,
        p8, p9, p10
    };
        for (int i = 0; i < 11; i++)

        {      
            array[i]->Voice();
        }

        for (int i = 0; i < 1; i++)

        {
            delete array[i];
        }

}
